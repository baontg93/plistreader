﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

namespace PlistReader
{
    class Program
    {
        static string[] ARR_EXT_DATA = new string[] { ".xml", ".plist" };
        static void Main(string[] args)
        {
            args = args.Length.Equals(0) ? GetAllFiles(ARR_EXT_DATA) : args;
            args = args.OrderBy(i=>i).ToArray();
            foreach (var file in args)
            {
                switch (Path.GetExtension(file).ToLower())
                {
                    case ".plist":
                        Spritesheet.LoadPlist(file);
                        break;
                    case ".xml":
                        Spritesheet.LoadXml(file);
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("\n\nPress anykey to close");
            Console.ReadKey();
        }

        static string[] GetAllFiles(string[] stringFormat = null)
        {
            string[] imgFormat = stringFormat;
            DirectoryInfo dirInfo = new DirectoryInfo(Directory.GetCurrentDirectory());
            List<FileInfo> listFiles = dirInfo.GetFiles().ToList();

            if (stringFormat != null && stringFormat.Length > 0)
            {
                for (int index = listFiles.Count - 1; index >= 0; index--)
                {
                    if (!stringFormat.Contains(Path.GetExtension(listFiles[index].FullName).ToLower()))
                    {
                        listFiles.RemoveAt(index);
                    }
                }
            }
            return listFiles.Select(f => f.FullName).OrderBy(x => x).ToArray();
        }

    }

    public class Spritesheet
    {
        public string Name { get; set; }
        public Dictionary<string, Image> Sprites { get; set; }

        public static void LoadPlist(string coordinatesFile)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(coordinatesFile);
            XmlNode metadata = doc.SelectSingleNode("/plist/dict/key[.='metadata']");
            XmlNode realTextureFileName = metadata.NextSibling.SelectSingleNode("key[.='realTextureFileName']");
            string spritesheetName = realTextureFileName.NextSibling.InnerText;
            Image spriteSheetImage = Image.FromFile(spritesheetName);
            XmlNode frames = doc.SelectSingleNode("/plist/dict/key[.='frames']");
            XmlNodeList list = frames.NextSibling.SelectNodes("key");

            Console.WriteLine(">>>>>>>>>>> Extracting file " + spritesheetName);
            foreach (XmlNode node in list)
            {
                XmlNode dict = node.NextSibling;
                string strRectangle;
                string strOffset;
                string strSourceSize;
                bool isRotate;

                try
                {
                    strRectangle = dict.SelectSingleNode("key[.='frame']").NextSibling.InnerText;
                    strOffset = dict.SelectSingleNode("key[.='offset']").NextSibling.InnerText;
                    strSourceSize = dict.SelectSingleNode("key[.='sourceSize']").NextSibling.InnerText;
                    isRotate = bool.Parse(dict.SelectSingleNode("key[.='rotated']").NextSibling.Name);
                }
                catch (Exception)
                {
                    strRectangle = dict.SelectSingleNode("key[.='textureRect']").NextSibling.InnerText;
                    strOffset = dict.SelectSingleNode("key[.='spriteOffset']").NextSibling.InnerText;
                    strSourceSize = dict.SelectSingleNode("key[.='spriteSourceSize']").NextSibling.InnerText;
                    isRotate = bool.Parse(dict.SelectSingleNode("key[.='textureRotated']").NextSibling.Name);
                }

                Rectangle frame = parseRectangle(strRectangle, isRotate);
                Point offset = parsePoint(strOffset, isRotate);
                Rectangle sourceRectangle = new Rectangle(0, 0, frame.Width, frame.Height);
                Point size = parsePoint(strSourceSize, isRotate);

                string spriteFrameName = node.InnerText;
                Bitmap sprite = new Bitmap(size.X, size.Y);

                using (Graphics g = Graphics.FromImage(sprite))
                {
                    g.CompositingMode = CompositingMode.SourceCopy;

                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    g.CompositingQuality = CompositingQuality.HighQuality;

                    g.SmoothingMode = SmoothingMode.HighQuality;

                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    g.DrawImage(spriteSheetImage, sourceRectangle, frame, GraphicsUnit.Pixel);
                }

                Console.WriteLine("\t" + spriteFrameName.Split('\\').Last().Split('/').Last());

                string output = Path.Combine(spritesheetName.Split('.')[0], spriteFrameName);
                string folderOutput = Path.GetDirectoryName(output);
                if (!Directory.Exists(folderOutput))
                {
                    Directory.CreateDirectory(folderOutput);
                }

                if (isRotate)
                {
                    //create an object that we can use to examine an image file
                    using (Image img = (Image)sprite)
                    {
                        //rotate the picture by 90 degrees and re-save the picture as a Jpeg
                        img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        sprite = new Bitmap(img);
                    }
                }

                sprite.Save(output);
            }
        }

        public static void LoadXml(string coordinatesFile)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(coordinatesFile);

            string spritesheetName = doc.GetElementsByTagName("texture")[0].Attributes["file"].Value;
            Image spriteSheetImage = Image.FromFile(spritesheetName);

            Console.WriteLine(">>>>>>>>>>> Extracting file " + spritesheetName);

            // Get and display all the book titles.
            XmlElement root = doc.DocumentElement;
            XmlNodeList list = root.GetElementsByTagName("textureregion");

            foreach (XmlNode node in list)
            {
                XmlNode dict = node.NextSibling;
                string strRectangle;
                string strOffset;
                string strSourceSize;
                bool isRotate;

                strRectangle = string.Format("{{{{{0},{1}}},{{{2},{3}}}}}", node.Attributes["x"].Value, node.Attributes["y"].Value, node.Attributes["width"].Value, node.Attributes["height"].Value);
                strOffset = string.Format("{{{0},{1}}}", node.Attributes["srcx"].Value, node.Attributes["srcy"].Value);
                strSourceSize = string.Format("{{{0},{1}}}", node.Attributes["srcwidth"].Value, node.Attributes["srcheight"].Value);
                isRotate = bool.Parse(node.Attributes["rotated"].Value);

                Rectangle frame = parseRectangle(strRectangle, isRotate);
                Point offset = parsePoint(strOffset, isRotate);
                Rectangle sourceRectangle = new Rectangle(0, 0, frame.Width, frame.Height);
                Point size = parsePoint(strSourceSize, isRotate);

                string spriteFrameName = node.Attributes["src"].Value;
                Bitmap sprite = new Bitmap(size.X, size.Y);

                using (Graphics g = Graphics.FromImage(sprite))
                {
                    g.CompositingMode = CompositingMode.SourceCopy;

                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    g.CompositingQuality = CompositingQuality.HighQuality;

                    g.SmoothingMode = SmoothingMode.HighQuality;

                    g.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    g.DrawImage(spriteSheetImage, sourceRectangle, frame, GraphicsUnit.Pixel);
                }

                Console.WriteLine("\t" + spriteFrameName.Split('\\').Last().Split('/').Last());

                string output = Path.Combine(spritesheetName.Split('.')[0], spriteFrameName);
                string folderOutput = Path.GetDirectoryName(output);
                if (!Directory.Exists(folderOutput))
                {
                    Directory.CreateDirectory(folderOutput);
                }

                if (isRotate)
                {
                    //create an object that we can use to examine an image file
                    using (Image img = (Image)sprite)
                    {
                        //rotate the picture by 90 degrees and re-save the picture as a Jpeg
                        img.RotateFlip(RotateFlipType.Rotate270FlipNone);
                        sprite = new Bitmap(img);
                    }
                }

                sprite.Save(output);
            }

            Console.ReadKey();
        }

        private static Rectangle parseRectangle(string rectangle, bool isRotate = false)
        {
            Regex expression = new Regex(@"\{\{(\d+),(\d+)\},\{(\d+),(\d+)\}\}");
            Match match = expression.Match(rectangle);
            if (match.Success)
            {
                int x = int.Parse(match.Groups[1].Value);
                int y = int.Parse(match.Groups[2].Value);
                int w = int.Parse(match.Groups[3].Value);
                int h = int.Parse(match.Groups[4].Value);
                return isRotate ? new Rectangle(x, y, h, w) :  new Rectangle(x, y, w, h);
            }
            return Rectangle.Empty;
        }

        private static Point parsePoint(string point, bool isRotate = false)
        {
            Regex expression = new Regex(@"\{(\d+),(\d+)\}");
            Match match = expression.Match(point);
            if (match.Success)
            {
                int x = int.Parse(match.Groups[1].Value);
                int y = int.Parse(match.Groups[2].Value);
                return isRotate ? new Point(y, x) : new Point(x, y);
            }
            return Point.Empty;
        }
    }
}
